/*
 * Helper functions for test programs.
 * Copyright © 2012, 2022 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEST_COMMON_H_
#define TEST_COMMON_H_

struct option;

void test_print_version(const char *program);
void test_print_options(const struct option *lopts);

size_t test_decode_hex(const char *hex, unsigned char *buf, size_t n);

#endif
