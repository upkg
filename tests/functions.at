# Copyright © 2012, 2022 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

AT_SETUP([upkg_decode_index])
AT_KEYWORDS([core function])

# various encodings of zero
m4_foreach_w([INDEX], [00 4000 408000 40808000 4080808000],
  [AT_CHECK([decodeindex INDEX], [0], [0
])
])

# truncated encodings of zero
m4_foreach_w([INDEX], ['' 40 4080 408080 40808080],
  [AT_CHECK([decodeindex INDEX], [1], [ignore], [ignore])
])

# overlong encoding of zero
AT_CHECK([decodeindex 408080808000], [1], [ignore], [ignore])

AT_CLEANUP

AT_SETUP([pcx_write_scanline run-length encoding])
AT_KEYWORDS([engine function])

AT_CHECK([pcxrle 00], [0], [00
])
AT_CHECK([pcxrle 0000], [0], [c200
])
AT_CHECK([pcxrle c100], [0], [c1c100
])

zero1=00
zero4=$zero1$zero1$zero1$zero1
zero16=$zero4$zero4$zero4$zero4
zero63=$zero16$zero16$zero16$zero4$zero4$zero4$zero1$zero1$zero1
zero64=$zero16$zero16$zero16$zero16
zero192=$zero64$zero64$zero64

AT_CHECK([# maximum possible run
pcxrle $zero63], [0], [ff00
])
AT_CHECK([# one more than maximum
pcxrle $zero64], [0], [ff0000
])
AT_CHECK([# consecutive runs
pcxrle $zero192], [0], [ff00ff00ff00c300
])

AT_CLEANUP
