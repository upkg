/*
 * upkg: tool for manipulating Unreal Tournament packages.
 * Copyright © 2009-2011, 2022 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef U_OBJECT_PACKAGE_H_
#define U_OBJECT_PACKAGE_H_

#include <glib-object.h>

#define U_PKG_TYPE (u_pkg_get_type())
#define U_PKG(obj) \
	G_TYPE_CHECK_INSTANCE_CAST(obj, U_PKG_TYPE, UPkg)
#define U_PKG_CLASS(class) \
	G_TYPE_CHECK_CLASS_CAST(class, U_PKG_TYPE, UPkgClass)
#define IS_U_PKG(obj) \
	G_TYPE_CHECK_INSTANCE_TYPE(obj, U_PKG_TYPE)
#define IS_U_PKG_CLASS(class) \
	G_TYPE_CHECK_CLASS_TYPE(class, U_PKG_TYPE, UPkgClass)
#define U_PKG_GET_CLASS(obj) \
	G_TYPE_INSTANCE_GET_CLASS(obj, U_PKG_TYPE, UPkgClass)

typedef struct UPkg      UPkg;
typedef struct UPkgClass UPkgClass;

struct UPkg {
	GTypeModule parent;
	struct upkg *pkg;
};

struct UPkgClass {
	GTypeModuleClass parent;
};

GType u_pkg_get_type(void);
GTypeModule *u_pkg_open(const char *name);

int u_pkg_is_native(GTypeModule *pkg);

#endif
