/*
 *  Functions for handling UObject package search paths.
 *  Copyright © 2009-2011 Nick Bowler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef U_OBJECT_VFS_H_
#define U_OBJECT_VFS_H_

#include <upkg.h>

/*
 * Insert a local package to the VFS.  A "local package" is an explicit
 * association of a name to a file, and thus can be located outside the normal
 * search path.  Local packages are searched before any other location.
 *
 * Returns a pointer to an internal copy of name on success, or NULL on
 * failure.  The returned name must not be modified or freed.  If name is
 * NULL, it is determined automatically from the filename.
 */
const char *u_pkg_vfs_add_local(const char *name, const char *file);

/*
 * Remove a local package from the VFS by name.
 */
void u_pkg_vfs_del_local(const char *name);

/*
 * Set the global VFS search path - a LT_PATHSEP-delimited sequence of
 * directories to be searched for packages after local packages.
 *
 * Returns 0 on success, or -1 on failure.
 */
int u_pkg_vfs_set_search_path(const char *path);

/*
 * Appends a directory to the global VFS search path.  This directory will
 * be searched after any directories already in the path.
 *
 * Returns 0 on success, or -1 on failure.
 */
int u_pkg_vfs_add_search_dir(const char *path);

/*
 * Get the global VFS search path.
 */
const char *u_pkg_vfs_get_search_path(void);

/*
 * Opens a package file by name.  First, local packages are searched for a
 * match.  If a local match is found, the global search path is never
 * consulted.  If no local match is found, the directories in the global search
 * path are examined, in order.
 *
 * When searching the global search path, the first file found which both
 * matches the given name and can be opened successfully is used.  A file
 * foo.EXT matches the name if foo is equal to name (without regard to case)
 * and EXT is one of u, utx, uax, umx or unr.  If multiple extensions
 * are valid, they are tried in this order.
 *
 * Note that the particular file extension used does not make any difference
 * as to what sort of objects can be contained in a package.
 *
 * Returns the opened package on success, or NULL on failure.
 */
struct upkg *u_pkg_vfs_open_by_name(const char *name);

/*
 * Initialize the UObject VFS system.  Returns 0 on success, -1 otherwise.
 * The VFS system can be safely initialized multiple times.
 */
int u_pkg_vfs_init(void);

/*
 * Shutdown the UObject VFS system.
 * The VFS system is only shut down when this function has been called as
 * many times as u_pkg_vfs_init.
 */
void u_pkg_vfs_exit(void);

#endif
