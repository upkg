/*
 *  upkg: tool for manipulating Unreal Tournament packages.
 *  Copyright © 2009-2011 Nick Bowler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef U_OBJECT_H_
#define U_OBJECT_H_

#include <glib-object.h>
#include <stdarg.h>
#include "upkg.h"

#define U_TYPE_OBJECT u_object_get_type()
#define U_OBJECT(obj) \
	G_TYPE_CHECK_INSTANCE_CAST(obj, U_TYPE_OBJECT, UObject)
#define U_OBJECT_CLASS(class) \
	G_TYPE_CHECK_CLASS_CAST(class, U_TYPE_OBJECT, UObjectClass)
#define IS_U_OBJECT(obj) \
	G_TYPE_CHECK_INSTANCE_TYPE(obj, U_TYPE_OBJECT)
#define IS_U_OBJECT_CLASS(class) \
	G_TYPE_CHECK_CLASS_TYPE(class, U_TYPE_OBJECT, UObjectClass)
#define U_OBJECT_GET_CLASS(obj) \
	G_TYPE_INSTANCE_GET_CLASS(obj, U_TYPE_OBJECT, UObjectClass)

typedef struct UObject      UObject;
typedef struct UObjectClass UObjectClass;

struct UObject {
	GObject parent;

	GTypeModule *pkg;
	struct upkg_file *pkg_file;
	char *pkg_name;
};

struct UObjectClass {
	GObjectClass parent;

	int (*deserialize)(UObject *obj);
};

GType u_object_get_type(void);

int u_object_deserialize(GObject *obj, GTypeModule *pkg, unsigned long idx);

float u_unpack_binary32_le(const unsigned char *buf);

GObject *u_object_new_from_package(GTypeModule *pkg, unsigned long idx);

/* Resolve an object reference from one object to another. */
GObject *u_object_get_by_link(GObject *obj, long link);

/* Logging helpers for UObject class implementations. */
void u_vlog_full(GObject *o, GLogLevelFlags level, const char *fmt, va_list ap);
void u_log_full(GObject *o, GLogLevelFlags level, const char *fmt, ...);

#define u_log(uo, ...)  u_log_full(G_OBJECT(uo), G_LOG_LEVEL_MESSAGE, __VA_ARGS__)
#define u_warn(uo, ...) u_log_full(G_OBJECT(uo), G_LOG_LEVEL_WARNING, __VA_ARGS__)
#define u_err(uo, ...)  u_log_full(G_OBJECT(uo), G_LOG_LEVEL_CRITICAL, __VA_ARGS__)

#endif
