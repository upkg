/*
 *  upkg: tool for manipulating Unreal Tournament packages.
 *  Copyright © 2009-2011 Nick Bowler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef U_OBJECT_EXPORTABLE_H_
#define U_OBJECT_EXPORTABLE_H_

#include <stdio.h>
#include <uobject/uobject.h>

#define U_TYPE_OBJECT_EXPORTABLE (u_object_exportable_get_type())
#define U_OBJECT_EXPORTABLE(obj) G_TYPE_CHECK_INSTANCE_CAST(obj, \
	U_TYPE_OBJECT_EXPORTABLE, UObjectExportableIface)
#define U_OBJECT_IS_EXPORTABLE(obj) G_TYPE_CHECK_INSTANCE_TYPE(obj, \
	U_TYPE_OBJECT_EXPORTABLE)
#define U_OBJECT_EXPORTABLE_GET_INTERFACE(inst) G_TYPE_INSTANCE_GET_INTERFACE \
	(inst, U_TYPE_OBJECT_EXPORTABLE, UObjectExportableIface)

typedef struct UObjectExportableIface UObjectExportableIface;

/*
 * Interface for UObjects that support exporting to a file.  For example, a
 * texture might be saved to disk as a PNG image for editing.
 */
struct UObjectExportableIface {
	GTypeInterface parent;

	int (*export)(UObject *obj, FILE *f);
	int (*export_name)(UObject *obj, char *buf, size_t n);
};

GType u_object_exportable_get_type(void);

/*
 * Export the object by writing it to the given FILE handle, which must have
 * been opened for writing in binary mode.
 */
int u_object_export(GObject *obj, FILE *f);

/*
 * Get the suggested output filename.  Not more than n-1 characters of the
 * filename will be written to buf, and the result is always null terminated.
 * Returns the number of characters that would be written if the buffer was
 * not bounded.  It is valid to call this function with a NULL buffer and a 0
 * size in order to determine how large the buffer needs to be.
 */
int u_object_export_name(GObject *obj, char *buf, size_t n);

#endif
