/*
 *  upkg: tool for manipulating Unreal Tournament packages.
 *  Copyright © 2009-2011 Nick Bowler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef U_OBJECT_MODULE_H_
#define U_OBJECT_MODULE_H_

#include <glib-object.h>

/*
 * Initialize the UObject module system.  Returns 0 on success, -1 otherwise.
 * The module system can be safely initialized multiple times.
 */
int u_object_module_init(void);

/*
 * Shutdown the UObject module system.  Returns 0 on success, -1 otherwise.
 * The module system is only shut down when this function has been called as
 * many times as u_object_module_init.
 */
int u_object_module_exit(void);

/*
 * Get a class handle from the UObject module system, where class is an
 * object reference for the class within the provided package.  This will load
 * the appropriate shared object if necessary.  The resulting handle can then
 * be used to create instances of the class with g_object_new.
 *
 * Returns 0 on failure.
 */
GType u_object_module_get_class(GTypeModule *pkg, long class);

#endif
