/*
 *  upkg: tool for manipulating Unreal Tournament packages.
 *  Copyright © 2009-2011 Nick Bowler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef U_OBJECT_LOADABLE_H_
#define U_OBJECT_LOADABLE_H_

#include <uobject/uobject.h>

#define U_TYPE_OBJECT_LOADABLE (u_object_loadable_get_type())
#define U_OBJECT_LOADABLE(obj) G_TYPE_CHECK_INSTANCE_CAST(obj, \
	U_TYPE_OBJECT_LOADABLE, UObjectLoadableIface)
#define U_OBJECT_IS_LOADABLE(obj) G_TYPE_CHECK_INSTANCE_TYPE(obj, \
	U_TYPE_OBJECT_LOADABLE)
#define U_OBJECT_LOADABLE_GET_INTERFACE(inst) G_TYPE_INSTANCE_GET_INTERFACE \
	(inst, U_TYPE_OBJECT_LOADABLE, UObjectLoadableIface)

typedef struct UObjectLoadableIface UObjectLoadableIface;

/*
 * Interface for UObjects supporting "load" and "unload".  These are generally
 * objects that need to allocate large amounts of memory in order to be
 * useful -- textures and sounds, for example -- where you might want to keep
 * around instances but the actual texture or sound data is not needed.
 */
struct UObjectLoadableIface {
	GTypeInterface parent;

	int (*load)(UObject *obj);
	void (*unload)(UObject *obj);
};

GType u_object_loadable_get_type(void);

int u_object_load(GObject *obj);
void u_object_unload(GObject *obj);

#endif
