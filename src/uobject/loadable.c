/*
 * upkg: tool for manipulating Unreal Tournament packages.
 * Copyright © 2009-2011, 2022 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>
#include "loadable.h"

GType u_object_loadable_get_type(void)
{
	static GType type = 0;
	if (type == 0) {
		static const GTypeInfo info = {
			sizeof (UObjectLoadableIface),
			NULL,
			NULL,
		};

		type = g_type_register_static(G_TYPE_INTERFACE,
			"UObjectLoadable", &info, 0);
	}
	return type;
}

int u_object_load(GObject *go)
{
	g_return_val_if_fail(IS_U_OBJECT(go), -1);
	g_return_val_if_fail(U_OBJECT_IS_LOADABLE(go), -1);

	return U_OBJECT_LOADABLE_GET_INTERFACE(go)->load(U_OBJECT(go));
}

void u_object_unload(GObject *go)
{
	g_return_if_fail(IS_U_OBJECT(go));
	g_return_if_fail(U_OBJECT_IS_LOADABLE(go));

	U_OBJECT_LOADABLE_GET_INTERFACE(go)->unload(U_OBJECT(go));
}
