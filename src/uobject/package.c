/*
 * upkg: tool for manipulating Unreal Tournament packages.
 * Copyright © 2009-2011, 2022 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <glib-object.h>
#include <ltdl.h>

#include <uobject/package.h>
#include <uobject/vfs.h>
#include "upkg.h"

#define U_PKG_GET_PRIV(o) \
	G_TYPE_INSTANCE_GET_PRIVATE(o, U_PKG_TYPE, struct upkg_priv)

struct upkg_priv {
	lt_dlhandle native;
};

G_DEFINE_TYPE(UPkg, u_pkg, G_TYPE_TYPE_MODULE);

static char *str_cpy_lower(char *dst, const char *src)
{
	size_t i;

	for (i = 0; src[i]; i++)
		dst[i] = tolower(src[i]);
	dst[i] = 0;

	return dst;
}

static void dl_print_errors(const char *prefix)
{
	const char *err;
	while ((err = lt_dlerror())) {
		if (prefix) fprintf(stderr, "%s: ", prefix);
		fprintf(stderr, "%s\n", err);
	}
}

static gboolean u_pkg_load(GTypeModule *m)
{
	struct upkg_priv *priv = U_PKG_GET_PRIV(m);
	int (*init_func)(GTypeModule *);

	/* Ignore failure here until we get rid of native-only packages. */
	U_PKG(m)->pkg = u_pkg_vfs_open_by_name(m->name);

	priv->native = lt_dlopenext(m->name);
	if (priv->native) {
		init_func = lt_dlsym(priv->native, "init");
		if (!init_func || init_func(m) != 0) {
			dl_print_errors(__func__);
			lt_dlclose(priv->native);
			upkg_close(U_PKG(m)->pkg);
			return FALSE;
		}
	}

	return TRUE;
}

static void u_pkg_unload(GTypeModule *m)
{
	struct upkg_priv *priv = U_PKG_GET_PRIV(m);
	void (*exit_func)(GTypeModule *);
	UPkg *upkg = U_PKG(m);

	if (priv->native) {
		exit_func = lt_dlsym(priv->native, "exit");
		if (exit_func) {
			exit_func(m);
		}

		if (lt_dlclose(priv->native) != 0) {
			dl_print_errors(__func__);
		}

		priv->native = NULL;
	}

	if (upkg->pkg) {
		upkg_close(upkg->pkg);
		upkg->pkg = NULL;
	}
}

static void u_pkg_init(UPkg *pkg)
{

}

static void u_pkg_finalize(GObject *o)
{
	u_pkg_unload(G_TYPE_MODULE(o));
}

static void u_pkg_class_init(UPkgClass *class)
{
	GTypeModuleClass *modclass = G_TYPE_MODULE_CLASS(class);
	GObjectClass *objclass = G_OBJECT_CLASS(class);

	const char *modpath = getenv("UOBJECT_MODULE_PATH");

	g_type_class_add_private(class, sizeof (struct upkg_priv));

	if (lt_dlinit() != 0) {
		dl_print_errors(__func__);
	}

	if (modpath && lt_dlsetsearchpath(modpath) != 0) {
		dl_print_errors(__func__);
	}
	if (lt_dladdsearchdir(PKGLIBDIR) != 0) {
		dl_print_errors(__func__);
	}

	modclass->load     = u_pkg_load;
	modclass->unload   = u_pkg_unload;
	objclass->finalize = u_pkg_finalize;
}

GTypeModule *u_pkg_open(const char *name)
{
	g_return_val_if_fail(name != NULL, NULL);

	char *pkgname = malloc(strlen(name)+1);
	if (!pkgname) {
		return NULL;
	}

	GTypeModule *mod = g_object_new(U_PKG_TYPE, NULL);
	if (!mod) {
		free(pkgname);
		return NULL;
	}

	mod->name = str_cpy_lower(pkgname, name);
	return mod;
}

int u_pkg_is_native(GTypeModule *m)
{
	struct upkg_priv *priv = U_PKG_GET_PRIV(m);

	return !!priv->native;
}
