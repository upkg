/*
 * upkg: tool for manipulating Unreal Tournament packages.
 * Copyright © 2009-2012, 2022 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <getopt.h>

#include <glib-object.h>

#include "upkg.h"
#include <uobject/uobject.h>
#include <uobject/exportable.h>
#include <uobject/loadable.h>
#include <uobject/module.h>
#include <uobject/package.h>
#include <uobject/vfs.h>

#include "help.h"

enum {
	MODE_INFO,
	MODE_LIST,
	MODE_EXPORT,
	MODE_MAX
};

int verbose = 0;

static const char *progname = "upkg";

#include "upkgopts.h"
static const char sopts[] = SOPT_STRING;
static const struct option lopts[] = { LOPTS_INITIALIZER, {0} };

static void print_version(void)
{
	printf("%s\n", PACKAGE_STRING);
	puts("Copyright (C) 2022 Nick Bowler.");
	puts("License GPLv3+: GNU GPL version 3 or any later version.");
	puts("This is free software: you are free to change and redistribute it.");
	puts("There is NO WARRANTY, to the extent permitted by law.");
}

static void print_usage(FILE *f)
{
	fprintf(f, "Usage: %s [options] [object ...]\n", progname);
	if (f != stdout)
		fprintf(f, "Try %s --help for more information.\n", progname);
}

static void print_help(void)
{
	const struct option *opt;

	print_usage(stdout);

	putchar('\n');
	puts("Options:");
	for (opt = lopts; opt->name; opt++) {
		struct lopt_help help;

		if (!lopt_get_help(opt, &help))
			continue;

		help_print_option(opt, help.arg, help.desc, 20);
	}
	putchar('\n');

	puts("For more information, see the upkg(1) man page.");
	putchar('\n');

	printf("Report bugs to <%s>.\n", PACKAGE_BUGREPORT);
}

static void print_upkg_flags(const char *prefix, unsigned long flags)
{
	if (flags & UPKG_FLAG_ALLOW_DOWNLOAD)
		printf("%sAllowDownload\n", prefix);
	if (flags & UPKG_FLAG_CLIENT_OPTIONAL)
		printf("%sClientOptional\n", prefix);
	if (flags & UPKG_FLAG_SERVER_ONLY)
		printf("%sServerOnly\n", prefix);
	if (flags & UPKG_FLAG_BROKEN_LINKS)
		printf("%sBrokenLinks\n", prefix);
	if (flags & UPKG_FLAG_INSECURE)
		printf("%sInsecure\n", prefix);
	if (flags & UPKG_FLAG_REQUIRED)
		printf("%sRequired\n", prefix);
}

static void print_upkg_object_flags(const char *prefix, unsigned long flags)
{
	if (flags & UPKG_OBJ_FLAG_TRANSACTIONAL)
		printf("%sTransactional\n", prefix);
	if (flags & UPKG_OBJ_FLAG_UNREACHABLE)
		printf("%sUnreachable\n", prefix);
	if (flags & UPKG_OBJ_FLAG_PUBLIC)
		printf("%sPublic\n", prefix);
	if (flags & UPKG_OBJ_FLAG_TAG_IMPORT)
		printf("%sImport\n", prefix);
	if (flags & UPKG_OBJ_FLAG_TAG_EXPORT)
		printf("%sTagExport\n", prefix);
	if (flags & UPKG_OBJ_FLAG_SOURCE_MODIFIED)
		printf("%sSourceModified\n", prefix);
	if (flags & UPKG_OBJ_FLAG_TAG_GARBAGE)
		printf("%sTagGarbage\n", prefix);
	if (flags & UPKG_OBJ_FLAG_NEED_LOAD)
		printf("%sNeedLoad\n", prefix);
	if (flags & UPKG_OBJ_FLAG_HIGHLIGHT_NAME)
		printf("%sHighlightName\n", prefix);
	if (flags & UPKG_OBJ_FLAG_IN_SINGULAR_FUNC)
		printf("%sInSingularFunc\n", prefix);
	if (flags & UPKG_OBJ_FLAG_SUPPRESSED)
		printf("%sSuppressed\n", prefix);
	if (flags & UPKG_OBJ_FLAG_IN_END_STATE)
		printf("%sInEndState\n", prefix);
	if (flags & UPKG_OBJ_FLAG_TRANSIENT)
		printf("%sTransient\n", prefix);
	if (flags & UPKG_OBJ_FLAG_PRELOADING)
		printf("%sPreloading\n", prefix);
	if (flags & UPKG_OBJ_FLAG_LOAD_FOR_CLIENT)
		printf("%sLoadForClient\n", prefix);
	if (flags & UPKG_OBJ_FLAG_LOAD_FOR_SERVER)
		printf("%sLoadForServer\n", prefix);
	if (flags & UPKG_OBJ_FLAG_LOAD_FOR_EDIT)
		printf("%sLoadForEdit\n", prefix);
	if (flags & UPKG_OBJ_FLAG_STANDALONE)
		printf("%sStandalone\n", prefix);
	if (flags & UPKG_OBJ_FLAG_NOT_FOR_CLIENT)
		printf("%sNotForClient\n", prefix);
	if (flags & UPKG_OBJ_FLAG_NOT_FOR_SERVER)
		printf("%sNotForServer\n", prefix);
	if (flags & UPKG_OBJ_FLAG_NOT_FOR_EDIT)
		printf("%sNotForEdit\n", prefix);
	if (flags & UPKG_OBJ_FLAG_DESTROYED)
		printf("%sDestroyed\n", prefix);
	if (flags & UPKG_OBJ_FLAG_NEED_POST_LOAD)
		printf("%sNeedPostLoad\n", prefix);
	if (flags & UPKG_OBJ_FLAG_HAS_STACK)
		printf("%sHasStack\n", prefix);
	if (flags & UPKG_OBJ_FLAG_NATIVE)
		printf("%sNative\n", prefix);
	if (flags & UPKG_OBJ_FLAG_MARKED)
		printf("%sMarked\n", prefix);
	if (flags & UPKG_OBJ_FLAG_ERROR_SHUTDOWN)
		printf("%sErrorShutdown\n", prefix);
	if (flags & UPKG_OBJ_FLAG_DEBUG_POST_LOAD)
		printf("%sDebugPostLoad\n", prefix);
	if (flags & UPKG_OBJ_FLAG_DEBUG_SERIALIZE)
		printf("%sDebugSerialize\n", prefix);
	if (flags & UPKG_OBJ_FLAG_DEBUG_DESTROY)
		printf("%sDebugDestroy\n", prefix);
}

static void print_guid(unsigned char *guid)
{
	for (unsigned i = 0; i < 16; i++) {
		printf("%02hhx", guid[i]);
		if (i == 15)
			putchar('\n');
		else
			putchar(' ');
	}
}

static void export_print_name(const struct upkg_export *e)
{
	if (e) {
		export_print_name(e->parent);
		printf(".%s", e->name);
	}
}

static void
export_print_fullname(GTypeModule *pkg, const struct upkg_export *export)
{
	printf("%s", pkg->name);
	export_print_name(export);
}

static void import_print_fullname(const struct upkg_import *import)
{
	if (import) {
		import_print_fullname(import->parent);
		if (import->parent)
			putchar('.');
		printf("%s", import->name);
	}
}

static void
export_print_class(GTypeModule *pkg, const struct upkg_export *export)
{
	struct upkg *upkg = U_PKG(pkg)->pkg;

	if (export->class < 0) {
		const struct upkg_import *class;

		class = upkg_get_import(upkg, -(export->class+1));
		import_print_fullname(class);
	} else if (export->class > 0) {
		const struct upkg_export *class;

		class = upkg_get_export(upkg, export->class-1);
		export_print_fullname(pkg, class);
	} else {
		printf("Core.Class");
	}
}

void print_upkg_exports(GTypeModule *pkg)
{
	struct upkg *upkg = U_PKG(pkg)->pkg;

	for (unsigned i = 0; i < upkg->export_count; i++) {
		const struct upkg_export *export = upkg_get_export(upkg, i);

		printf("%u - ", i+1);
		export_print_fullname(pkg, export);
		printf(" (");
		export_print_class(pkg, export);
		printf(")\n");

		printf("  Flags: %lx\n", export->flags);
		if (verbose >= 2) {
			print_upkg_object_flags("    ", export->flags);
		}
	}
}

void print_upkg_imports(struct upkg *pkg)
{
	for (unsigned i = 0; i < pkg->import_count; i++) {
		const struct upkg_import *import = upkg_get_import(pkg, i);

		printf("%u - ", i);
		import_print_fullname(import);
		printf(" (%s.%s)\n", import->class_package, import->class_name);
	}
}

int package_info(GTypeModule *pkg)
{
	struct upkg *upkg = U_PKG(pkg)->pkg;

	printf("Version: %u\n", upkg->version);
	printf("License: %u\n", upkg->license);
	printf("GUID: ");
	print_guid(upkg->guid);

	printf("Flags:   %lx\n", upkg->flags);
	if (verbose >= 2) {
		print_upkg_flags("        ", upkg->flags);
	}

	printf("Names:   %lu\n", upkg->name_count);
	if (verbose >= 3) {
		for (unsigned long i = 0; i < upkg->name_count; i++) {
			printf("%lu - %s\n", i, upkg_get_name(upkg, i));
		}
	}

	printf("Exports: %lu\n", upkg->export_count);
	if (verbose >= 1) print_upkg_exports(pkg);
	printf("Imports: %lu\n", upkg->import_count);
	if (verbose >= 1) print_upkg_imports(upkg);

	return 0;
}

static int package_list(GTypeModule *pkg, long current)
{
	struct upkg *upkg = U_PKG(pkg)->pkg;
	const struct upkg_export *parent = NULL;

	if (current >= 0) {
		parent = upkg_get_export(upkg, current);
		assert(parent != NULL);
	}

	for (unsigned i = 0; i < upkg->export_count; i++) {
		const struct upkg_export *export = upkg_get_export(upkg, i);

		if (export->parent != parent)
			continue;

		export_print_fullname(pkg, export);

		if (verbose >= 1) {
			printf(" (");
			export_print_class(pkg, export);
			printf(")");
		}

		putchar('\n');
	}

	return 0;
}

static int object_dump_properties(GTypeModule *pkg, unsigned long idx)
{
	GParamSpec **props;
	GObject *obj;
	unsigned n;

	obj = u_object_new_from_package(pkg, idx);
	if (!obj) {
		fprintf(stderr, "%s: failed to load object.\n", progname);
		return -1;
	}

	props = g_object_class_list_properties(G_OBJECT_GET_CLASS(obj), &n);
	for (unsigned i = 0; i < n; i++) {
		GValue val = {0};
		char *valstr;

		printf("  property %s:", props[i]->name);
		fflush(stdout);

		g_value_init(&val, props[i]->value_type);
		g_object_get_property(obj, props[i]->name, &val);

		if (G_VALUE_HOLDS(&val, U_TYPE_OBJECT)) {
			UObject *obj = g_value_get_object(&val);

			if (obj && obj->pkg_name) {
				printf(" %s", obj->pkg_name);
			}
		}

		valstr = g_strdup_value_contents(&val);
		printf(" %s\n", valstr);
		g_free(valstr);
	}

	g_free(props);
	g_object_unref(obj);

	return 0;
}

static int object_dump_offsets(GTypeModule *pkg, unsigned long idx)
{
	struct upkg_file *f;

	f = upkg_export_open(U_PKG(pkg)->pkg, idx);
	if (!f) {
		fprintf(stderr, "%s: failed to open export.\n", progname);
		return -1;
	}

	printf("  file size: %lu\n", f->len);
	printf("  file start: %#lx\n", f->base);
	printf("  file end: %#lx\n", f->base + f->len);

	upkg_export_close(f);

	return 0;
}

static int object_info(GTypeModule *pkg, unsigned long idx)
{
	struct upkg *upkg = U_PKG(pkg)->pkg;
	const struct upkg_export *export;
	int ret = 0;

	export = upkg_get_export(upkg, idx);
	export_print_fullname(pkg, export);

	printf(" (");
	export_print_class(pkg, export);
	printf(")\n");

	if (verbose >= 1 && object_dump_properties(pkg, idx) != 0)
		ret = -1;

	if (verbose >= 3 && object_dump_offsets(pkg, idx))
		ret = -1;

	return ret;
}

static int export(GTypeModule *pkg, GObject *obj, unsigned idx)
{
	struct upkg *upkg = U_PKG(pkg)->pkg;
	char name[256];
	FILE *of;
	int rc;

	if (U_OBJECT_IS_LOADABLE(obj) && u_object_load(obj) != 0) {
		fprintf(stderr, "%s: failed to load export data.\n", progname);
		return -1;
	}

	rc = u_object_export_name(obj, name, sizeof name);
	if (rc <= 0) {
		/* XXX: We could use a default name here. */
		fprintf(stderr, "%s: failed to determine export filename.\n",
		                progname);
		return -1;
	}

	printf("exporting ");
	export_print_fullname(pkg, upkg_get_export(upkg, idx));
	printf(" to %s\n", name);

	of = fopen(name, "wb");
	if (!of) {
		perror(name);
		return -1;
	}

	u_object_export(obj, of);

	if (fclose(of) != 0) {
		perror(name);
		return -1;
	}

	if (U_OBJECT_IS_LOADABLE(obj)) {
		u_object_unload(obj);
	}

	return 0;
}

static int object_export(GTypeModule *pkg, unsigned long idx)
{
	GObject *obj;
	int ret = -1;

	obj = u_object_new_from_package(pkg, idx);
	if (!obj) {
		fprintf(stderr, "%s: failed to load object.\n", progname);
		return -1;
	}

	if (!U_OBJECT_IS_EXPORTABLE(obj)) {
		fprintf(stderr, "%s: object is not exportable.\n", progname);
		goto out;
	}

	ret = export(pkg, obj, idx);
out:
	g_object_unref(obj);
	return ret;
}

int package_export(GTypeModule *pkg)
{
	struct upkg *upkg = U_PKG(pkg)->pkg;
	GObject *obj;
	int ret = 0;

	for (unsigned i = 0; i < upkg->export_count; i++) {
		obj = u_object_new_from_package(pkg, i);
		if (U_OBJECT_IS_EXPORTABLE(obj) && export(pkg, obj, i) != 0) {
			ret = -1;
		}
		g_object_unref(obj);
	}

	return ret;
}

static int process_object(int mode, const char *objname)
{
	char work[strlen(objname)+1], *p = work, *c;
	GTypeModule *pkg;
	long current = -1;
	int ret = 0;

	strcpy(work, objname);

	/* First, find and open the actual package. */
	c = strchr(p, '.');
	if (c) *c = 0;

	pkg = u_pkg_open(p);
	if (!pkg || !g_type_module_use(pkg)) {
		fprintf(stderr, "%s: %s: failed to open package.\n",
		                progname, work);
		goto out;
	}

	if (!U_PKG(pkg)->pkg) {
		if (u_pkg_is_native(pkg)) {
			fprintf(stderr, "%s: %s: not a UObject package.\n",
		                        progname, work);
		} else {
			fprintf(stderr, "%s: %s: package not found.\n",
			                progname, work);
		}

		goto out_unuse;
	}

	/* Resolve the hierarchical reference. */
	while (c) {
		p = c+1;
		c = strchr(p, '.');
		if (c) *c = 0;

		current = upkg_export_find(U_PKG(pkg)->pkg, current, p);
		if (current == -1) {
			/* We want to print the full name up to this point. */
			strcpy(work, objname);
			if (c) *c = 0;

			fprintf(stderr, "%s: %s: object not found.\n",
			                progname, work);
			goto out_unuse;
		}
	}

	switch (mode) {
	case MODE_INFO:
		if (current < 0)
			ret = package_info(pkg);
		else
			ret = object_info(pkg, current);
		break;
	case MODE_LIST:
		ret = package_list(pkg, current);
		break;
	case MODE_EXPORT:
		if (current < 0)
			ret = package_export(pkg);
		else
			ret = object_export(pkg, current);
		break;
	default:
		abort();
	}

out_unuse:
	g_type_module_unuse(pkg);
out:
	return ret;
}

int main(int argc, char **argv)
{
	const char *pkgname = NULL;
	unsigned mode = MODE_INFO;
	int opt, rc;

	if (argc > 0) progname = argv[0];

	if (u_pkg_vfs_init() != 0)
		return EXIT_FAILURE;
	if (u_object_module_init() != 0)
		return EXIT_FAILURE;

	while ((opt = getopt_long(argc, argv, sopts, lopts, NULL)) != -1) {
		switch (opt) {
		case 'f':
			pkgname = u_pkg_vfs_add_local(NULL, optarg);
			if (!pkgname) {
				fprintf(stderr, "failed to add package `%s'.\n", optarg);
				return EXIT_FAILURE;
			}
			break;
		case 'i':
			mode = MODE_INFO;
			break;
		case 'l':
			mode = MODE_LIST;
			break;
		case 'x':
			mode = MODE_EXPORT;
			break;
		case 'v':
			verbose++;
			break;
		case 'V':
			print_version();
			return EXIT_SUCCESS;
		case 'H':
			print_help();
			return EXIT_SUCCESS;
		default:
			print_usage(stderr);
			return EXIT_FAILURE;
		}
	}

	if (!pkgname && !argv[optind]) {
		print_usage(stderr);
		return EXIT_FAILURE;
	}

	rc = EXIT_SUCCESS;
	if (argv[optind]) {
		for (int i = optind; i < argc; i++) {
			if (process_object(mode, argv[i]) != 0)
				rc = EXIT_FAILURE;
		}
	} else {
		if (process_object(mode, pkgname) != 0)
			rc = EXIT_FAILURE;
	}

	u_object_module_exit();
	u_pkg_vfs_exit();
	return rc;
}
