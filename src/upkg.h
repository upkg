/*
 *  upkg: tool for manipulating Unreal Tournament packages.
 *  Copyright © 2009-2011 Nick Bowler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UPKG_H_
#define UPKG_H_

#include <stddef.h>

#define UPKG_FLAG_ALLOW_DOWNLOAD  0x0001
#define UPKG_FLAG_CLIENT_OPTIONAL 0x0002
#define UPKG_FLAG_SERVER_ONLY     0x0004
#define UPKG_FLAG_BROKEN_LINKS    0x0008
#define UPKG_FLAG_INSECURE        0x0010
#define UPKG_FLAG_REQUIRED        0x8000

#define UPKG_OBJ_FLAG_TRANSACTIONAL    0x00000001
#define UPKG_OBJ_FLAG_UNREACHABLE      0x00000002
#define UPKG_OBJ_FLAG_PUBLIC           0x00000004
#define UPKG_OBJ_FLAG_TAG_IMPORT       0x00000008
#define UPKG_OBJ_FLAG_TAG_EXPORT       0x00000010
#define UPKG_OBJ_FLAG_SOURCE_MODIFIED  0x00000020
#define UPKG_OBJ_FLAG_TAG_GARBAGE      0x00000040
#define UPKG_OBJ_FLAG_NEED_LOAD        0x00000200
#define UPKG_OBJ_FLAG_HIGHLIGHT_NAME   0x00000400
#define UPKG_OBJ_FLAG_IN_SINGULAR_FUNC 0x00000800
#define UPKG_OBJ_FLAG_SUPPRESSED       0x00001000
#define UPKG_OBJ_FLAG_IN_END_STATE     0x00002000
#define UPKG_OBJ_FLAG_TRANSIENT        0x00004000
#define UPKG_OBJ_FLAG_PRELOADING       0x00008000
#define UPKG_OBJ_FLAG_LOAD_FOR_CLIENT  0x00010000
#define UPKG_OBJ_FLAG_LOAD_FOR_SERVER  0x00020000
#define UPKG_OBJ_FLAG_LOAD_FOR_EDIT    0x00040000
#define UPKG_OBJ_FLAG_STANDALONE       0x00080000
#define UPKG_OBJ_FLAG_NOT_FOR_CLIENT   0x00100000
#define UPKG_OBJ_FLAG_NOT_FOR_SERVER   0x00200000
#define UPKG_OBJ_FLAG_NOT_FOR_EDIT     0x00400000
#define UPKG_OBJ_FLAG_DESTROYED        0x00800000
#define UPKG_OBJ_FLAG_NEED_POST_LOAD   0x01000000
#define UPKG_OBJ_FLAG_HAS_STACK        0x02000000
#define UPKG_OBJ_FLAG_NATIVE           0x04000000
#define UPKG_OBJ_FLAG_MARKED           0x08000000
#define UPKG_OBJ_FLAG_ERROR_SHUTDOWN   0x10000000
#define UPKG_OBJ_FLAG_DEBUG_POST_LOAD  0x20000000
#define UPKG_OBJ_FLAG_DEBUG_SERIALIZE  0x40000000
#define UPKG_OBJ_FLAG_DEBUG_DESTROY    0x80000000

#define UPKG_HDR_MAGIC 0x9e2a83c1
#define UPKG_HDR_SIZE  36

struct upkg_file_ops {
	size_t (*read)(void *buf, size_t size, void *handle);
	int    (*seek)(void *handle, long offset, int whence);
	long   (*tell)(void *handle);
	int    (*eof) (void *handle);
};

struct upkg {
	unsigned version, license;
	unsigned char guid[16];
	unsigned long flags;

	unsigned long name_count, export_count, import_count;
};

struct upkg_export {
	const struct upkg_export *parent;

	const char *name;
	unsigned long flags;
	long class;
};

struct upkg_import {
	const struct upkg_import *parent;

	const char *name;
	const char *class_package, *class_name;
};

struct upkg_file {
	const char *name;

	int eof;

	struct upkg *pkg;
	unsigned long base, offset, len;
};

/* Default I/O operations for ordinary files. */
extern const struct upkg_file_ops upkg_default_fops;

struct upkg *upkg_open(void *handle, const struct upkg_file_ops *fops,
                       int (*destructor)(void *handle));
struct upkg *upkg_fopen(const char *path);
int upkg_close(struct upkg *pkg);

const char *upkg_get_name(struct upkg *pkg, unsigned long idx);
const struct upkg_export *upkg_get_export(struct upkg *pkg, unsigned long idx);
const struct upkg_import *upkg_get_import(struct upkg *pkg, unsigned long idx);

long upkg_export_find(struct upkg *pkg, long parent, const char *name);

struct upkg_file *upkg_export_open(struct upkg *pkg, unsigned long idx);
size_t upkg_export_read(struct upkg_file *f, void *buf, size_t n);
int    upkg_export_seek(struct upkg_file *f, long offset, int whence);
long   upkg_export_tell(struct upkg_file *f);
void   upkg_export_close(struct upkg_file *f);

size_t upkg_decode_index(long *val, const unsigned char *bytes, size_t n);

#endif
