/*
 *  upkg: tool for manipulating Unreal Tournament packages.
 *  Copyright © 2009-2011 Nick Bowler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MUSIC_MODULE_H_
#define MUSIC_MODULE_H_

#include "upkg.h"

struct music_mod;

/* Initialize the module decoding system. */
int music_mod_init(void);
/* Shutdown the module decoding system. */
void music_mod_exit(void);

struct music_mod *music_mod_open(struct upkg_file *f);
void music_mod_close(struct music_mod *mod);
int music_mod_dump(struct music_mod *mod, FILE *of);

const char *music_mod_type(struct music_mod *mod);

#endif
