/*
 * upkg: a tool for manipulating Unreal Tournament packages.
 * Very simple PCX writer for saving extracted textures.
 *
 * Copyright © 2012 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENGINE_PCX_H_
#define ENGINE_PCX_H_

#include <engine/palette.h>

struct pcx_head {
	unsigned short width, height;
	unsigned char encoded[128];
};

int pcx_init_header(struct pcx_head *head);
int pcx_write_scanline(const struct pcx_head *head, const unsigned char *src, FILE *f);
int pcx_write_palette(EnginePalette *palette, FILE *f);

#endif
