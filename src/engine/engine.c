/*
 * upkg: tool for manipulating Unreal Tournament packages.
 * Copyright © 2009-2012, 2020, 2022 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>
#include <glib-object.h>

#include <engine/mesh.h>
#include <engine/music.h>
#include <engine/palette.h>
#include <engine/sound.h>
#include <engine/texture.h>

#define init engine_LTX_init
#define exit engine_LTX_exit

int init(GTypeModule *m)
{
	engine_mesh_register_type(m);
	engine_music_register_type(m);
	engine_palette_register_type(m);
	engine_sound_register_type(m);
	engine_texture_register_type(m);
	return 0;
}

void exit(void)
{
}
